# Travailler en équipe avec git 
## Logique

## Branches

### Des branches : pourquoi? 
Schéma + gitflow
### Créer des branches


### Voir les branches créées 


### Supprimer des branches
git push origin :feature_name

git branch -d feature_name

### Faire le ménage : 
Ecrire un bel historique
git rebase -i origin/master

## Récupérer les informations 
git fetch
git pull

git status

## Contributions



### pull requests

### merge requests


## Retour vers le passé

git log

git checkout "commit"
